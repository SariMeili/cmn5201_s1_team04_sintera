// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#include "Sinterra_PrototypeGameMode.h"
#include "Sinterra_PrototypeCharacter.h"
#include "UObject/ConstructorHelpers.h"

ASinterra_PrototypeGameMode::ASinterra_PrototypeGameMode()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/ThirdPersonCPP/Blueprints/ThirdPersonCharacter"));
	if (PlayerPawnBPClass.Class != NULL)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}
}
